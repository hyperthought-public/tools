"""
Centralized access to HyperThought API.
"""

import collections
from datetime import datetime
from enum import Enum
import json
import os

import requests

import utils
from utils import FOLDER_TYPE


# Any status codes >= the following will result in errors being reported.
ERROR_THRESHOLD = 300


class HyperThoughtAPIException(Exception):
    pass


class GenericAPI:
    """
    Base class for app-specific API classes.

    Parameters
    ----------
    auth : auth.Authorization
        Authorization manager for access to HyperThought™ over OIDC.
    """

    def __init__(self, auth):
        self._auth = auth
        self._base_url = auth.get_base_url()

    def _report_api_error(self, response):
        """
        Raise an exception with data on the error.

        Parameters
        ----------
        response : requests.models.Response
            A response object correponding to an API request.
            The status code will likely indicate an error, but the caller will
            be responsible for checking it.
        """
        TRUNCATION_LENGTH = 255

        try:
            error = json.dumps(response.json())
            # Only non-JSON results will be truncated.
            truncated = False
        except:
            error = str(response.content)
            
            if len(error) > TRUNCATION_LENGTH:
                error = error[:TRUNCATION_LENGTH] + "..."
                truncated = True
            else:
                truncated = False

        if truncated:
            error_label = "Error (truncated)"
        else:
            error_label = "Error"

        exception_msg = (
            f"Status code: {response.status_code}.  "
            f"{error_label}: {error}"
        )
        raise HyperThoughtAPIException(exception_msg)


class FilesAPI(GenericAPI):
    """
    Files API switchboard.

    Contains methods that (roughly) correspond to endpoints for the
    HyperThought files app.  The methods simplify some tasks, such as
    uploading and downloading files.

    Parameters
    ----------
    auth : auth.Authorization
        Authorization object used to get headers and cookies needed to call
        HyperThought endpoints.
    """

    class FileType(Enum):
        """
        Enum describing types of file documents to be returned from methods.
        
        See the get_from_location method for an example.
        """
        # FILES_ONLY = 0
        FOLDERS_ONLY = 1
        FILES_AND_FOLDERS = 2

    def __init__(self, auth):
        super().__init__(auth)
        self._backend = None
        
    def get_document(self, id):
        """
        Get a database document for a file, given its id.

        Parameters
        ----------
        id : str
            The database id for a file or folder.

        Returns
        -------
        A dict-like database document for the file with the given id.
        """
        r = requests.get(
            url='{}/api/files/'.format(self._base_url),
            headers=self._auth.get_headers(),
            cookies=self._auth.get_cookies(),
            params={'id': id,},
            verify=False,
        )

        if r.status_code < ERROR_THRESHOLD:
            return r.json()
        else:
            self._report_api_error(response=r)
    
    def get_from_location(self, space=None, space_id=None, path=None,
                          file_type=None):
        """
        Get HyperThought files/folders from a specific location.

        Parameters
        ----------
        space : str or None
            The space of interest.  Must be 'group', 'project', or 'user'.
            If None, will default to 'user'.
        space_id : str or None
            The id of a group or project, or the username for a user.
            If None, will default to the user's username.
        path : str or None
            The id path to the location of interest.  If none, will default to
            id root path (e.g., ',').
            Ex: an id path for '/path/to/folder' would have the form
                ',uuid,uuid,uuid,'
        file_type : FileType or None
            An enum value for the type of files to get.  A None value will
            default to FileType.FILES_AND_FOLDERS.

        Returns
        -------
        A list of documents (dicts) from the database corresponding to
        files/folders at the specified path in the specified space.
        """
        # Validate parameters.
        space = self._validate_space(space)
        space_id = self._validate_space_id(space, space_id)
        path = self._validate_path(path)
        file_type = self._validate_file_type(file_type)

        files_url = '{}/api/files/'.format(self._base_url)
        headers = self._auth.get_headers()
        cookies = self._auth.get_cookies()
        params = {'path': path}

        if space == 'project':
            params['method'] = 'project_files'
            params['project'] = space_id
        elif space == 'group':
            params['method'] = 'group_files'
            params['group'] = space_id
        else:
            params['method'] = 'user_files'

        if file_type == self.FileType.FOLDERS_ONLY:
            params['type'] = FOLDER_TYPE

        r = requests.get(files_url, headers=headers, cookies=cookies,
                         params=params, verify=False)
        
        if r.status_code >= ERROR_THRESHOLD:
            self._report_api_error(response=r)

        output = r.json()
        
        # TODO:  Make sure this is necessary.
        if output is None:
            output = []

        # TODO:  Make sure this is necessary.
        if not isinstance(output, list):
            output = [output]

        return output

    def get_id(self, name, space=None, space_id=None, path=None):
        """
        Get an id for a file/folder with a given name at a given location.

        Parameters
        ----------
        name : str
            The name of the file system entry.
        space : str or None
            The space of interest.  Must be 'group', 'project', or 'user'.
            If None, will default to 'user'.
        space_id : str or None
            The id of a group or project, or the username for a user.
            If None, will default to the user's username.
        path : str or None
            The id path to the location of interest.  If none, will default to
            id root path (e.g., ',').
            Ex: an id path for '/path/to/folder' would have the form
                ',uuid,uuid,uuid,'

        Returns
        -------
        An id, if the specified file/folder exists, else None.
        """
        # Validate parameters.
        name = self._validate_name(name)
        space = self._validate_space(space)
        space_id = self._validate_space_id(space, space_id)
        path = self._validate_path(path)

        files_url = '{}/api/files/'.format(self._base_url)
        headers = self._auth.get_headers()
        cookies = self._auth.get_cookies()
        params = {
            'path': path,
            'search': name,
        }

        if space == 'project':
            params['method'] = 'project_files'
            params['project'] = space_id
        elif space == 'group':
            params['method'] = 'group_files'
            params['group'] = space_id
        else:
            params['method'] = 'user_files'

        r = requests.get(files_url, headers=headers, cookies=cookies,
                         params=params, verify=False)

        if r.status_code >= ERROR_THRESHOLD:
            # NOTE:  This method will throw an exception.
            self._report_api_error(response=r)

        if not r.json():
            return None

        for document in r.json():
            if document['content']['name'] == name:
                # Return the first match found.
                # Ideally, there should be only one.
                return document['content']['pk']

        return None

    def create_folder(self, name, space=None, space_id=None, path=None):
        """
        Create a folder in HyperThought.

        Parameters
        ----------
        name : str
            The name of the folder to create.
        space : str or None
            The space of interest.  Must be 'group', 'project', or 'user'.
            If None, will default to 'user'.
        space_id : str or None
            The id of a group or project, or the username for a user.
            If None, will default to the user's username.
        path : str or None
            The id path to the location of interest.  If none, will default to
            id root path (e.g., ',').
            Ex: an id path for '/path/to/folder' would have the form
                ',uuid,uuid,uuid,'

        Returns
        -------
        The id of the new folder.
        """
        name = self._validate_name(name)
        space = self._validate_space(space)
        space_id = self._validate_space_id(space, space_id)
        path = self._validate_path(path)

        url = '{}/api/files/create-folder/'.format(self._auth.get_base_url())
        headers = self._auth.get_headers()
        cookies = self._auth.get_cookies()
        request_data = {
            'space': space,
            'space_id': space_id,
            'path': path,
            'name': name,
        }

        r = requests.post(url, headers=headers, cookies=cookies,
                          json=request_data, verify=False)

        if r.status_code >= ERROR_THRESHOLD:
            self._report_api_error(response=r)

        folder_id = r.json()['document']['content']['pk']
        return folder_id

    def upload(self, local_path, space=None, space_id=None, path=None,
               metadata=None):
        """
        Upload a file to HyperThought.

        Parameters
        ----------
        local_path : str
            The path to a file on the local system.
        space : str or None
            The space of interest.  Must be 'group', 'project', or 'user'.
            If None, will default to 'user'.
        space_id : str or None
            The id of a group or project, or the username for a user.
            If None, will default to the user's username.
        path : str or None
            The id path to the location of interest.  If none, will default to
            id root path (e.g., ',').
            Ex: an id path for '/path/to/folder' would have the form
                ',uuid,uuid,uuid,'
        metadata : dict-like or None
            Metadata for the file.

        Returns
        -------
        A tuple containing the file id and the name of the file.  (The name is
        returned in case it is changed by HyperThought™ to ensure uniqueness.)
        """
        # Validate parameters.
        local_path = self._validate_local_path(local_path)
        space = self._validate_space(space)
        space_id = self._validate_space_id(space, space_id)
        path = self._validate_path(path)
        metadata = self._validate_metadata(metadata)

        # Get file name and size using the local path.
        active_local_path = utils.get_active_path(local_path)
        name = active_local_path.split(os.path.sep)[-1]
        size = os.path.getsize(active_local_path)
        
        # Get an upload url.
        url, file_id = self._get_upload_url(
            space=space,
            space_id=space_id,
            name=name,
            size=size,
            path=path,
            metadata=metadata,
        )

        # Use the url to upload the file.
        self._upload_using_url(url, active_local_path)

        # Move the file from the temporary to the permanent file collection.
        file_name = self._temp_to_perm(file_id)

        # Return the file id.
        return (file_id, file_name,)

    def download(self, file_id, directory):
        """
        Download a file from HyperThought to the local file system.

        Parameters
        ----------
        file_id : str
            The HyperThought id for a file to be downloaded.
        directory : str
            A local directory path to which the file will be downloaded.
        """
        # Validate parameters.
        self._validate_id(file_id)
        self._validate_local_path(directory)

        # Make sure the path is a directory.
        active_directory_path = utils.get_active_path(directory)
        assert os.path.isdir(active_directory_path)

        # Get the file name.
        file_document = self.get_document(id=file_id)
        file_name = file_document['content']['name']
        file_path = os.path.join(active_directory_path, file_name)

        # Get a download url.
        url = self._get_download_url(file_id)

        # Use the url to download the file.
        self._download_using_url(url, file_path)

    def delete(self, id):
        """
        Delete a file or folder.

        Parameters
        ----------
        id : str
            The id of the file/folder to be deleted.
        """
        # Validate parameters.
        id = self._validate_id(id)

        r = requests.delete(
            url='{}/api/files/'.format(self._base_url),
            headers=self._auth.get_headers(),
            cookies=self._auth.get_cookies(),
            json={'id': id,},
            verify=False,
        )

        if r.status_code >= ERROR_THRESHOLD:
            # NOTE:  This method will throw an exception.
            self._report_api_error(response=r)

    def get_backend(self):
        """
        Get the files backend.
        
        Returns
        -------
        A string describing the file backend, e.g. 's3' or 'default'.
        """
        if self._backend is not None:
            return self._backend
        
        url = f'{self._base_url}/api/files/backend/'
        headers = self._auth.get_headers()
        cookies = self._auth.get_cookies()
        r = requests.get(url=url, headers=headers, cookies=cookies,
                         verify=False)

        if r.status_code >= ERROR_THRESHOLD:
            self._report_api_error(response=r)

        self._backend = r.json()['backend']
        # TODO:  Replace assertion with proper error handling.
        assert self._backend in ('s3', 'default',)
        return self._backend

    def is_folder(self, document):
        """Determine whether a document represents a folder in the
        HyperThought file system."""
        if not isinstance(document, collections.Mapping):
            return False

        if 'content' not in document:
            return False

        if 'ftype' not in document['content']:
            return False

        return document['content']['ftype'] == FOLDER_TYPE

    def _get_upload_url(self, space, space_id, name, size, path=None,
                        metadata=None):
        """
        Get presigned url to upload a file.

        Called from self.upload_file.

        Parameters
        ----------
        space : str
            The type of space.  Must 'group', 'project', or 'user'.
        space_id : str or None
            The id for a group or project.  Irrelevant for user spaces.
        name : str
            The name of the folder to create.
        size : int
            The size of the file in bytes.
        path : str
            The path to the directory that will contain the file.
        metadata : dict-like or None
            Metadata for the file.

        Returns
        -------
        A tuple contained the presigned url of interest as well as the file id
        for the file to be uploaded.
        """
        request_data = {
            'space': space,
            'space_id': space_id,
            'path': path,
            'name': name,
            'size': size,
            'metadata': metadata,
        }
        generate_url = '{}/api/files/generate-upload-url/'.format(
            self._base_url)
        headers = self._auth.get_headers()
        cookies = self._auth.get_cookies()
        r = requests.post(generate_url, headers=headers, cookies=cookies,
                          json=request_data, verify=False)

        if r.status_code >= ERROR_THRESHOLD:
            # NOTE:  This method will throw an exception.
            self._report_api_error(response=r)

        return r.json()['url'], r.json()['fileId']

    def _upload_using_url(self, upload_url, local_path):
        """
        Use a url to upload a file.

        Called from self.upload_file.

        Parameters
        ----------
        upload_url : str
            The url to which the file should be uploaded.
        local_path : str
            The local path to the file to be uploaded.
        """
        upload_url = self._validate_url(upload_url)
        local_path = self._validate_local_path(local_path)

        with open(local_path, 'rb') as file_handle:
            byte_data = file_handle.read()

        kwargs = {'url': upload_url, 'data': byte_data, 'verify': False,}

        if self.get_backend() == 'default':
            kwargs['headers'] = self._auth.get_headers()
            kwargs['cookies'] = self._auth.get_cookies()
        else:
            # TODO:  Why can't this be removed?
            kwargs['headers'] = {'Content-Type': 'application/octet-stream'}

        r = requests.put(**kwargs)

        if r.status_code >= ERROR_THRESHOLD:
            self._report_api_error(response=r)

    def _temp_to_perm(self, file_id):
        """
        Move a file from the temporary (invisible) to the permanent (visible)
        file collection after the file has been completely uploaded.

        Parameters
        ----------
        id : str
            The HyperThought id for the file.
        """
        update_url = '{}/api/files/temp-to-perm/'.format(self._base_url)
        headers = self._auth.get_headers()
        cookies = self._auth.get_cookies()
        request_data = {'file_ids': [file_id]}
        r = requests.patch(update_url, headers=headers, cookies=cookies,
                           json=request_data, verify=False)

        if r.status_code >= ERROR_THRESHOLD:
            self._report_api_error(response=r)

        updated_files = r.json()['updated']

        if file_id in updated_files:
            return updated_files[file_id]

        return None

    def _get_download_url(self, file_id):
        """
        Get a url that can be used to download a file.

        Parameters
        ----------
        id : str
            The HyperThought id for a file of interest.

        Returns
        -------
        A url that can be used to download the file.
        """
        file_id = self._validate_id(file_id)

        generate_url = '{}/api/files/generate-download-url/'.format(
            self._base_url)
        headers = self._auth.get_headers()
        cookies = self._auth.get_cookies()
        params = {'id': file_id}

        r = requests.get(url=generate_url, headers=headers, cookies=cookies,
                         params=params, verify=False)

        if r.status_code >= ERROR_THRESHOLD:
            self._report_api_error(response=r)

        return r.json()['url']

    def _download_using_url(self, download_url, local_path):
        """
        Use a generated url to download a file.

        Parameters
        ----------
        download_url : str
            The generated url for downloading the file of interest.
            See self._get_download_url. 
        local_path : str
            The local system path where the downloaded file will be saved.
        """
        kwargs = {
            'url': download_url,
            'stream': True,
            'verify': False,
        }

        if self.get_backend == 'default':
            kwargs['headers'] = self._auth.get_headers()
            kwargs['cookies'] = self._auth.get_cookies()

        with requests.get(**kwargs) as r:
            r.raise_for_status()
            with open(local_path, 'wb') as f:
                for chunk in r.iter_content(chunk_size=8192):
                    if chunk:   # filter out keep-alive new chunks
                        f.write(chunk)

    def _validate_id(self, id_):
        assert isinstance(id_, str)
        return id_

    def _validate_space(self, space=None):
        if space is None:
            space = 'user'

        # TODO:  Call error-handling function instead of raising an AssertionError.
        #        Same goes for all assertions in validation methods.
        assert space in ('group', 'project', 'user',)

        return space

    def _validate_space_id(self, space=None, space_id=None):
        if space is None:
            space = self._validate_space(space)

        if space == 'user' and space_id is None:
            space_id = self._auth.get_username()

        assert isinstance(space_id, str)
        return space_id

    def _validate_path(self, path):
        if path is None:
            path = utils.ID_PATH_SEP

        assert isinstance(path, str)
        assert path.startswith(utils.ID_PATH_SEP)
        assert path.endswith(utils.ID_PATH_SEP)
        return path

    def _validate_name(self, name):
        assert isinstance(name, str)
        return name

    def _validate_size(self, size):
        assert isinstance(size, int) and size >= 0
        return size

    def _validate_metadata(self, metadata):
        # TODO:  Implement appropriate validation.
        # stub
        return metadata

    def _validate_url(self, url):
        # TODO:  Use regex?
        assert isinstance(url, str)
        return url

    def _validate_local_path(self, local_path):
        assert isinstance(local_path, str)
        assert os.path.exists(local_path)
        return local_path

    def _validate_file_type(self, file_type):
        if file_type is None:
            file_type = self.FileType.FILES_AND_FOLDERS

        assert isinstance(file_type, self.FileType)

        return file_type


class GroupsAPI(GenericAPI):
    """
    Groups API switchboard.

    Contains methods that correspond to endpoints for HyperThought™ groups.

    Parameters
    ----------
    auth : auth.Authorization
        Authorization object used to get headers and cookies needed to call
        HyperThought endpoints.
    """

    NAME_FIELD = 'name'

    def __init__(self, auth):
        super().__init__(auth)

    def get_groups(self):
        """Get groups available to the current user."""
        r = requests.get(
            url='{}/api/groups/'.format(self._auth.get_base_url()),
            headers=self._auth.get_headers(),
            cookies=self._auth.get_cookies(),
            verify=False,
        )

        if r.status_code < ERROR_THRESHOLD:
            return r.json()
        else:
            self._report_api_error(response=r)

    def get_name_field(self):
        """
        Get the key name of the field containing the group name.

        This will be a name in the content section of a group document.
        """
        return self.NAME_FIELD


class ProjectsAPI(GenericAPI):
    """
    Projects API switchboard.

    Contains methods that correspond to endpoints for HyperThought™ projects.

    Parameters
    ----------
    auth : auth.Authorization
        Authorization object used to get headers and cookies needed to call
        HyperThought endpoints.
    """

    NAME_FIELD = 'title'

    def __init__(self, auth):
        super().__init__(auth)

    def get_projects(self):
        """Get projects available to the current user."""
        r = requests.get(
            url='{}/api/projects/project/'.format(self._auth.get_base_url()),
            headers=self._auth.get_headers(),
            cookies=self._auth.get_cookies(),
            verify=False,
        )

        if r.status_code < ERROR_THRESHOLD:
            return r.json()
        else:
            self._report_api_error(response=r)

    def get_name_field(self):
        """
        Get the key name of the field containing the project name.

        This will be a name in the content section of a project document.
        """
        return self.NAME_FIELD


class MetatronAPI(GenericAPI):
    """
    Metatron (parsers) API switchboard.

    Contains methods that correspond to endpoints for HyperThought™ parsers.

    Parameters
    ----------
    auth : auth.Authorization
        Authorization object used to get headers and cookies needed to call
        HyperThought endpoints.
    """

    def __init__(self, auth):
        super().__init__(auth)

    def get_parsers(self):
        """Get information on parsers available to the current user."""
        r = requests.get(
            url=f'{self._base_url}/api/metatron/parsers/',
            headers=self._auth.get_headers(),
            cookies=self._auth.get_cookies(),
            verify=False,
        )

        if r.status_code < ERROR_THRESHOLD:
            return r.json()
        else:
            self._report_api_error(response=r)


class CommonAPI(GenericAPI):
    """
    Common API switchboard.

    Contains methods corresponding to endpoints that are called from multiple
    HyperThought™ components.

    Parameters
    ----------
    auth : auth.Authorization
        Authorization object used to get headers and cookies needed to call
        HyperThought endpoints.
    """

    class BugSeverity(Enum):
        """Severity levels used when reporting bugs."""
        LOW = 0
        MEDIUM = 1
        HIGH = 2
        CRITICAL = 3

        def to_string(self):
            """Convert enum instance to a string."""
            return self.name.title()

        @classmethod
        def to_enum(cls, severity):
            """
            Get an enum instance from corresponding text.
            
            Parameters
            ----------
            severity : str
                A string representing a severity level.
        
            Returns
            -------
            An enum instance.
            """
            # TODO:  Handle error case:  when severity string does not
            #        correspond to an enum value.
            return cls[severity.upper()]

    def __init__(self, auth):
        super().__init__(auth)

    def report_bug(self, description, severity=None):
        """
        Report a bug in the HyperThought™ API or a related application.

        Parameters
        ----------
        description : str
            A description of the error encountered.
        severity : BugSeverity or None
            The severity of the error being reported.  Default: MEDIUM.

        Returns
        -------
        A dictionary representing the bug report.  Keys will include the
        following:
            id : int
                The database id associated with the bug report record.
            user : str
                The username of the reported.
            email : str
                The email address of the reporter.
            description : str
                The description of the error, same value as the parameter.
            severity : str
                The severity level of the error, parameter value converted to
                title-case string.
            location : str
                A url supplied to the endpoint to identify which component
                the error is associated with.  The value used here is simply
                the base url plus "/api".
        """
        # Validate inputs.
        # TODO:  improve error handling, document exceptions in docstring.
        assert isinstance(description, str)
        if severity is None:
            severity = self.BugSeverity.MEDIUM
        assert isinstance(severity, self.BugSeverity)
        
        # Report the bug.
        location = f"{self._base_url}/api"
        url = f"{self._base_url}/api/common/user_incident/"
        data = {
            'user': self._auth.get_username(),
            'email': self._auth.get_email(),
            # This is a hack.  The location field expects a URL.
            'location': f"{self._base_url}/api",
            'description': description,
            'severity': severity.to_string(),
        }
        r = requests.post(
            url=url,
            data=data,
            headers=self._auth.get_headers(),
            cookies=self._auth.get_cookies(),
            verify=False,
        )

        if r.status_code < ERROR_THRESHOLD:
            return r.json()
        else:
            self._report_api_error(response=r)