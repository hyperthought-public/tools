-- upload.sql
-- SQL script to create a sqlite database corresponding to an upload job
-- executed via API/script.
-- Each job should have its own database.

-- NOTE:  This SQL script makes use of a data type (BOOLEAN) not available in
--        native sqlite.  The Python sqlite3 functions register_adapter and
--        register_converter should be executed to define these types prior to
--        script execution.  See upload_db.py.

-- Enforce referential integrity.
PRAGMA foreign_keys = ON;

-- Create a table to hold status values for file upload operations.
CREATE TABLE FileStatus (
    id              INTEGER PRIMARY KEY AUTOINCREMENT,
    status          TEXT,
    CONSTRAINT status UNIQUE (status)
);

-- Insert statuses into the table.
-- The "In Progress" status can be applied to a file or folder.  A folder
-- upload will be "In Progress" when files/folders contained within
-- it still need to be processed.
INSERT INTO FileStatus (status) VALUES ("In Progress");
-- The "Success" status can be applied to a file or folder.  A folder upload
-- will a "Success" when all files contained within it have been uploaded.
INSERT INTO FileStatus (status) VALUES ("Success");
-- The "Error" status can be applied to a file or folder.  When applied to a
-- folder, it will indicate an error creating the folder in HyperThought™, not
-- errors uploading files within the folder.
INSERT INTO FileStatus (status) VALUES ("Error");

-- Create a table to represent a file/folder being uploaded.
CREATE TABLE File (
    id              INTEGER PRIMARY KEY AUTOINCREMENT,
    file_status_id  INTEGER NOT NULL,
    path            TEXT NOT NULL,
    is_folder       BOOLEAN NOT NULL,
    is_top_level    BOOLEAN NOT NULL,
    created_on      DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    modified_on     DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    FOREIGN KEY(file_status_id) REFERENCES FileStatus(id),
    CONSTRAINT path UNIQUE (path)
);

CREATE INDEX idx__File__file_status_id ON File (file_status_id);
CREATE INDEX idx__File__path ON File (path);
CREATE INDEX idx__File__is_folder ON File (is_folder);

-- Create a table to store error text.
CREATE TABLE Error (
    id              INTEGER PRIMARY KEY AUTOINCREMENT,
    error           TEXT NOT NULL,
    CONSTRAINT error UNIQUE (error)
);

CREATE INDEX idx__Error__error ON Error (error);

-- Create a table to associate errors with attempts to upload files.
-- In relational database parlance, this is a junction table between File and
-- Error.
CREATE TABLE FileError (
    id              INTEGER PRIMARY KEY AUTOINCREMENT,
    file_id         INTEGER NOT NULL,
    error_id        INTEGER NOT NULL,
    occurred_on     DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    -- NOTE:  A new FileError record will be created every time an error is
    --        encountered.  Thus, there is no reason to add a modified_on field.
    FOREIGN KEY(file_id) REFERENCES File(id),
    FOREIGN KEY(error_id) REFERENCES Error(id)
);

CREATE INDEX idx__FileError__file_id ON FileError (file_id);
CREATE INDEX idx__FileError__error_id ON FileError (error_id);