"""
Create manifest for files to be uploaded to s3.

The manifest will consist of a pandas DataFrame having the following columns:
    -   path (index)
    -   size (in bytes)
    -   created_on
    -   modified_on

Command line arguments
----------------------
directory : REQUIRED
    The directory for which a manifest will be created.
output_file : OPTIONAL
    A path to the resulting manifest (csv) file.  Any existing file at that
    path will be overwritten.  Defaults to ./data/manifest.csv.
-   The source directory path.

NOTES
-----
On Windows, only absolute paths will be allowed.  In addition, network drives
will need to be mapped for this script to work.

IMPROVEMENTS
------------
-   Use csv instead of pandas.
-   Write one line at a time.
-   Use threads and/or async.
"""

import argparse
from datetime import datetime
import json
import os
import platform

import bunch
import pandas as pd


WINDOWS_PATH_PREFIX = '\\\\?\\'


def create_manifest(directory, top_level_blacklist=None):
    """
    Create a manifest of files contained within a given directory.

    (Only files will be included in the manifest, not folders.)

    Parameters
    ----------
    directory : str
        A path to the directory of interest.
    top_level_blacklist : list-like of str
        File system entries in the top level of the directory (not in a
        subdirectory) that are likely junk, and may result in permissions
        errors.

    Returns
    -------
    A pandas.DataFrame containing the following columns:
        -   path
        -   size (in bytes)
        -   created_on (the date the file was created, in ISO string format)
        -   modified_on (the date the file was last modified, in ISO string
            format)
    """
    # Trim whitespace.
    directory = directory.strip()

    if platform.system() == 'Windows':
        # Make sure drives are formatted appropriately.
        if directory[-1] == ':':
            directory += '\\'

        # Add the long path prefix.
        # From here, the path prefix will be propagated to all paths of interest.
        directory = WINDOWS_PATH_PREFIX + directory

    # Set the top-level blacklist.
    if top_level_blacklist is None:
        with open('manifest_top_level_blacklist.json') as fh:
            top_level_blacklist = json.load(fh)

    def _add_file(direntry):
        """Add a file to the manifest."""
        assert(direntry.is_file())
        dir_path = direntry.path
        path.append(dir_path)
        size.append(os.path.getsize(dir_path))
        created_on.append(str(datetime.fromtimestamp(os.path.getctime(dir_path))))
        modified_on.append(str(datetime.fromtimestamp(os.path.getmtime(dir_path))))

    def _add_directory(directory):
        """Add files in a directory to the manifest."""
        assert(directory.is_dir())
        for direntry in os.scandir(directory.path):
            if direntry.is_file():
                _add_file(direntry)
            # Test for is_dir to avoid symlinks.
            elif direntry.is_dir():
                _add_directory(direntry)
    
    def _create_pseudo_directory_direntry(path, isdir=True):
        """Create a duck type resembling DirEntry to wrap a path string.
        The resulting object will have a path member and is_dir method.
        This will be used for the top-level directory, since there appears
        to be no way to instantiate a DirEntry object directly.
        """
        
        def _is_dir():
            return isdir
            
        def _is_file():
            return not isdir
        
        return bunch.Bunch(
            path=path,
            is_dir=_is_dir,
            is_file=_is_file
        )

    # Define lists that will be used to create output DataFrame.
    path = []
    size = []
    created_on = []
    modified_on = []
    
    # Filter top-level items (files, dirs) to remove blacklisted items.
    items = [
        '{}{}{}'.format(directory, os.sep, subpath)
        for subpath in os.listdir(directory)
        if subpath not in top_level_blacklist
    ]
        
    # Call the recursive _add_directory inner function for each
    # top-level subdirectory.
    # The _add_directory function will append to the lists created above.
    for item in items:        
        if os.path.isdir(item):
            pseudo_direntry = _create_pseudo_directory_direntry(item, True)
            _add_directory(directory=pseudo_direntry)
        elif os.path.isfile(item):
            pseudo_direntry = _create_pseudo_directory_direntry(item, False)
            _add_file(direntry=pseudo_direntry)
            
    # Use the lists to create a DataFrame.
    dat = pd.DataFrame({
        'path': path,
        'size': size,
        'created_on': created_on,
        'modified_on': modified_on
    })
    dat.set_index('path', inplace=True)
    return dat


if __name__ == '__main__':
    # Parse command-line arguments.
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--directory',
        required=True,
        help=(
            'The directory for which a manifest should be created.'
        ),
        type=os.path.abspath
    )
    parser.add_argument(
        '--output_file',
        default='./data/manifest.csv',
        required=False,
        help=(
            'The path to the manifest file to be created.'
        ),
        type=os.path.abspath
    )
    args = parser.parse_args()

    # Create the manifest.
    df_manifest = create_manifest(args.directory)

    # Set encoding based on operating system.
    encoding = 'cp1250' if platform.system() == 'Windows' else 'utf-8'
    df_manifest.to_csv(args.output_file, encoding=encoding)
