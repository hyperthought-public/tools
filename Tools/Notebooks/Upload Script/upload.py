"""
upload.py

Used to upload files from a local system to HyperThought™.
"""

from collections import namedtuple
import os
import urllib

import api
import auth
import upload_db as db
from utils import ID_PATH_SEP


Location = namedtuple('Location', ['space', 'space_id', 'id_path'])


def _parse_location_from_url(location_url, auth):
    """
    Get space, space_id, and path from a HyperThought™ files url.

    Parameters
    ----------
    location_url : str
        A url corresponding to a location in the HyperThought™ file system.
    auth : auth.Authorization
        An authorization object used to manage access to HyperThought™ via OIDC.

    Returns
    -------
    A Location object containing information on the location that is parsed
    from the url.
    """
    ID_PATH_PARAM = 'load_at'
    query = urllib.parse.urlsplit(location_url).query
    params = urllib.parse.parse_qs(query)

    if 'project' in params:
        space = 'project'
        space_id = params['project'][0]
    elif 'group' in params:
        space = 'group'
        space_id = params['group'][0]
    else:
        space = 'user'
        space_id = auth.get_username()

    if ID_PATH_PARAM in params:
        id_path = params[ID_PATH_PARAM][0]
    else:
        id_path = ID_PATH_SEP

    return Location(space=space, space_id=space_id, id_path=id_path)


class Uploader:
    """
    Manager for upload operations.

    Parameters
    ----------
    auth : auth.Authentication
        An object that manages authentication to HyperThought™ via OIDC.
    job_name : str
        A name for an upload job.  Each job should have a unique name.
        A sqlite database will be created with the job name as its file name
        and 'db' as its extension.  For now, all such files will be created in
        the same directory as the rest of the code base.
    location_url : str
        A url corresponding to a location (path) in the HyperThought™ file
        system.  Either a location url or a combination of space, space_id,
        and id_path must be provided.
    space : str
        The type of file space.  Must be 'group', 'project', or 'user'.
    space_id : str
        The id of the space.  Will be a group or project id, or a username.
    id_path : str
        The comma-delimited path to the location of interest.  Consists of
        folder ids in the following format:  ',uuid,uuid,uuid,'.
    verbose : bool
        Determines whether status updates are printed to the console.
        Defaults to True.    

    Recommended Usage
    -----------------
    1.  Instantiate an Uploader object using an authentication object, job name,
        and location url.
    2.  Call upload for each file or folder to be uploaded to the specified
        location.
    """

    def __init__(self, auth, job_name, location_url=None,
                 space=None, space_id=None, id_path=None, verbose=True):
        # Give location_url priority, if provided.
        # The location info provided in the url will override any info
        # provided in the other parameters.
        if location_url is not None:
            location = _parse_location_from_url(location_url, auth)
            space = location.space
            space_id = location.space_id
            id_path = location.id_path

        # Validate inputs.
        if not space in ('group', 'project', 'user',):
            raise ValueError('invalid space')
        if not isinstance(space_id, str):
            raise ValueError('invalid space_id')
        if not isinstance(id_path, str):
            raise ValueError('invalid id_path')
        if not id_path.startswith(ID_PATH_SEP) or not id_path.endswith(ID_PATH_SEP):
            raise ValueError(f"id_path must begin and end with '{ID_PATH_SEP}'")

        self._space = space
        self._space_id = space_id
        self._base_id_path = id_path
        self._files_api = api.FilesAPI(auth=auth)
        self._db_connection = db.get_database_connection(job_name)
        self._db_file_manager = db.FileManager(
            db_connection=self._db_connection
        )
        self._db_error_manager = db.ErrorManager(
            db_connection=self._db_connection,
            file_manager=self._db_file_manager
        )

    def upload(self, local_path):
        """Upload a local file or folder to HyperThought™."""
        # Validate parameters.
        try:
            self._validate_local_path(local_path)
        except ValueError as e:
            print(str(e))
            print(f"{local_path} is not a valid path.")
            return

        if os.path.isfile(local_path):
            self._upload_file(
                local_path=local_path,
                id_path=self._base_id_path,
                top_level=True,
            )
        else:
            self._upload_folder(
                local_path=local_path,
                id_path=self._base_id_path,
                top_level=True,
            )

    def has_errors(self):
        """Determine whether errors occurred.  Returns boolean."""
        return self._db_file_manager.has_errors()

    def retry_errors(self):
        """Make another attempt to upload any files that encountered errors."""
        for path in self._db_file_manager.get_retry_paths():
            self.upload(path)

    def get_errors(self):
        """
        Get a data frame containing upload errors.

        Returns
        -------
        A pandas.DataFrame containing the following columns:
         -  path (local system file path)
         -  error (description of the error)
         -  occurred_on (a timestamp for when the error occurred)
        """
        return self._db_file_manager.get_errors()

    def get_log(self):
        """
        Get a log of files that were successfully uploaded.

        For now, folders will not be included in the log.

        Returns
        -------
        A pandas.DataFrame containing the following columns:
         -  path (local system file path)
        """
        return self._db_file_manager.get_log()

    def _upload_file(self, local_path, id_path, top_level=False):
        # Get or create the file.
        file_ = self._db_file_manager.get_file_from_path(local_path)

        if file_ is not None:
            if file_['file_status_id'] == db.FileStatus.SUCCESS.value:
                return

            file_id = file_['id']
        else:
            file_id = self._db_file_manager.create_file(
                path=local_path,
                top_level=top_level,
            )            

        # Attempt to upload the file.
        try:
            _, file_name = self._files_api.upload(
                local_path=local_path,
                space=self._space,
                space_id=self._space_id,
                path=id_path,
            )

            # TODO:  Handle possible change in file name.

            # Update the file status to "SUCCESS".
            self._db_file_manager.update_file_status(
                file_id=file_id,
                status=db.FileStatus.SUCCESS
            )

            print(f"{local_path} has been uploaded.")
        except Exception as e:
            # Get or create the error.
            error_record = self._db_error_manager.get_error(error)

            if not error_record:
                error_id = self._db_error_manager.create_error(
                    error=error,
                    commit=False
                )
            else:
                error_id = error_record['id']
            
            # Add the error to the file.
            self._db_error_manager.add_error_to_file(
                error_id=error_id,
                file_id=file_id,
                commit=False
            )

            # Update the file status to "ERROR".
            self._db_file_manager.update_file_status(
                file_id=file_id,
                status=db.FileStatus.ERROR,
                commit=True
            )
            print(f"\nError uploading {local_path}:\n\t{error}\n")

    def _upload_folder(self, local_path, id_path, top_level=False):
        # Get or create the file record for the folder.
        file_ = self._db_file_manager.get_file_from_path(local_path)

        if file_ is not None:
            if file_['file_status_id'] == db.FileStatus.SUCCESS.value:
                return

            file_id = file_['id']
        else:
            file_id = self._db_file_manager.create_file(
                path=local_path,
                top_level=top_level,
            )   

        # Get or create ht_id for folder.
        file_name = os.path.split(local_path)[-1]
        ht_folder_id = self._files_api.get_id(
            name=file_name,
            space=self._space,
            space_id=self._space_id,
            path=id_path
        )
        
        # TODO:  Change status to ERROR if the following operation is not
        #        successful.
        if ht_folder_id is None:
            ht_folder_id = self._files_api.create_folder(
                name=file_name,
                space=self._space,
                space_id=self._space_id,
                path=id_path,
            )

        child_id_path = f"{id_path}{ht_folder_id}{ID_PATH_SEP}"

        # Upload the contents of the folder.
        for dir_entry in os.scandir(local_path):
            if os.path.isfile(dir_entry.path):
                self._upload_file(
                    local_path=dir_entry.path,
                    id_path=child_id_path,
                )
            elif os.path.isdir(dir_entry.path):
                self._upload_folder(
                    local_path=dir_entry.path,
                    id_path=child_id_path,
                )

        # Update the status of the folder if everything was successful.
        if not self._db_file_manager.folder_has_errors(folder_path=local_path):
            self._db_file_manager.update_file_status(
                file_id,
                status=db.FileStatus.SUCCESS,
            )

    def _validate_local_path(self, local_path):
        if not os.path.exists(local_path):
            raise ValueError('local_path must be an existing path')
        
        if not os.path.isfile(local_path) and not os.path.isdir(local_path):
            raise ValueError('local_path must not be a symlink or mount point')

