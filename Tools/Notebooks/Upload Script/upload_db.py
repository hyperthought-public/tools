"""
upload_db.py

Database functionality related to actions taken in upload.py.
"""

from enum import Enum
import os
import shutil
import sqlite3
import string
import sys

import pandas as pd


TEMPLATE_DB = os.path.join('database_template', 'upload_template.db')
TEMPLATE_SQL_SCRIPT = os.path.join('database_template', 'upload.sql')
DATABASE_FOLDER = 'job_databases'

# Register adapter and converter for "BOOLEAN" type.
sqlite3.register_adapter(bool, int)
sqlite3.register_converter("BOOLEAN", lambda db_value: bool(int(db_value)))


class DatabaseExistsException(Exception):
    """
    Exception to be raised when a database already exists and a user opts out
    of opening the existing database.
    """
    pass


class FileStatus(Enum):
    """Enum corrsponding to status in FileStatus table."""
    IN_PROGRESS = 1
    SUCCESS = 2
    ERROR = 3
    TERMINATED = 4

    def display_text(self):
        """Translate a status name to displayable text."""
        return self.name.replace('_', ' ').title()


def create_template(db_path=TEMPLATE_DB):
    """
    Create a template database.

    The template database will have all the tables but no data (except in
    lookup tables).  When a job database needs to be created, the template
    database file will be copied.  (This operation is cheaper than executing
    the template SQL script.)

    Parameters
    ----------
    db_path : str
        The path to the template database file.
    """
    # Remove the template database file if it already exists.
    if os.path.exists(db_path):
        os.remove(db_path)

    # Open a connection to the database file.
    # Detect types like DATETIME, which will then be treated as datetime
    # objects instead of strings.
    with sqlite3.connect(db_path, detect_types=sqlite3.PARSE_DECLTYPES) as conn:
        # Allow values in records to be retrieved using column names as keys.
        conn.row_factory = sqlite3.Row

        # Enforce referential integrity.
        conn.execute("PRAGMA foreign_keys = ON")

        # Run the SQL script.
        with open(TEMPLATE_SQL_SCRIPT) as f:
            sql = f.read()

        conn.executescript(sql)
        conn.commit()


def validate_job_name(job_name):
    """
    Validate a job name.

    The job name should be a non-empty string containing only letters, numbers,
    and underscores.

    Parameters
    ----------
    job_name : str
        The name of an upload job.

    Exceptions
    ----------
    Raises ValueError if the job name does not conform to the rules specified
    above.
    """
    if not isinstance(job_name, str) or not job_name:
        raise ValueError("job_name must be a non-empty string")

    valid_characters = f"{string.ascii_letters}_{string.digits}"
    for character in job_name:
        if character not in valid_characters:
            raise ValueError((
                "Invalid character(s) in job name. "
                "Only letters, digits, and underscores are allowed."
            ))


def get_database_connection(job_name):
    """
    Get a connection to a database file.

    Parameters
    ----------
    job_name : str
        The name of an upload job.

    Returns
    -------
    A connection to the database file corresponding to the job.

    Exceptions
    ----------
    A DatabaseExistsException will be raised if a database already exists and
    the user opts out of opening it.
    """
    validate_job_name(job_name)
    
    if not os.path.exists(DATABASE_FOLDER):
        os.mkdir(DATABASE_FOLDER)

    db_file = os.path.join(DATABASE_FOLDER, f"{job_name}.db")

    # If the job database already exists, ask the user if s/he wants to continue.
    if os.path.exists(db_file):
        print(f"A database file ({db_file}) already exists for job {job_name}.")

        while True:
            choice = input("Do you wish to open the existing file?  (Y/n) ")
            choice = choice.lower()

            if not choice:
                break
            elif choice[0] == 'y':
                break
            elif choice[0] == 'n':
                raise DatabaseExistsException(f"{db_file} exists.")
    # Otherwise, create the database file by copying the template.
    else:
        shutil.copyfile(TEMPLATE_DB, db_file)
        print(f"Job database file created: {db_file}")

    # Open a connection to the database file.
    # Detect types like DATETIME, which will then be treated as datetime
    # objects instead of strings.
    conn = sqlite3.connect(db_file, detect_types=sqlite3.PARSE_DECLTYPES)

    # Allow values in records to be retrieved using column names as keys.
    conn.row_factory = sqlite3.Row

    # Enforce referential integrity.
    conn.execute("PRAGMA foreign_keys = ON")

    return conn


class FileManager:
    """
    Manager class for database operations relating to files.

    (Basically, a collection of functions with shared access to a database
    connection object.)

    Parameters
    ----------
    db_connection : sqlite3.Connection
        A connection to the database of interest.

    Exceptions
    ----------
    A ValueError will be thrown if the db_connection parameter cannot be
    validated.
    """

    def __init__(self, db_connection):
        if not isinstance(db_connection, sqlite3.Connection):
            raise ValueError("db_connection must be a sqlite3 connection")

        self._conn = db_connection

    def create_file(self, path, top_level, status=FileStatus.IN_PROGRESS, commit=True):
        """
        Create a file record in the database.

        Parameters
        ----------
        path : str
            A path to a file on the local system.
        top_level : bool
            True if the file/folder is not nested in another folder in the job,
            False otherwise.
        status : FileStatus
            The status for the file.
        commit : bool
            Determines whether changes will be committed to the database.
            This may be false if the changes are part of a transaction.

        Returns
        -------
        The id for the file, or None if an input validation error is encountered.

        Exceptions
        ----------
        -   ValueErrors will be thrown if the parameters cannot be validated.
        -   An IntegrityError will be thrown if the insert query violates
            integrity constaints.  (This may happen if a record already exists
            for the path, due to a uniqueness constraint.)
        """
        # Validate parameters.
        self._validate_path(path)
        self._validate_top_level(top_level)
        self._validate_status(status)

        is_folder = os.path.isdir(path)

        # Make sure the path is a canonical path.
        path = os.path.realpath(path)

        # Define and execute the relevant SQL statement.
        sql = """
        INSERT INTO File (path, file_status_id, is_folder, is_top_level)
        VALUES (:path, :file_status_id, :is_folder, :is_top_level)
        """
        cursor = self._conn.cursor()
        cursor.execute(sql, {
            'path': path,
            'file_status_id': status.value,
            'is_folder': is_folder,
            'is_top_level': top_level,
        })
        file_id = cursor.lastrowid

        if commit:
            self._conn.commit()

        return file_id

    def get_file_from_path(self, path):
        """
        Get a file record given a local file path.

        Parameters
        ----------
        path : str
            A path to a file or folder on the local system.

        Returns
        -------
        A row object corresponding to the file record of interest, if found;
        otherwise None.

        Exceptions
        ----------
        A ValueError will be thrown if the path cannot be validated.
        """
        self._validate_path(path)
        path = os.path.realpath(path)
        sql = "SELECT * FROM File WHERE path = :path"
        cursor = self._conn.cursor()
        cursor.execute(sql, {'path': path})
        row = cursor.fetchone()

        if not row:
            return None

        return row

    def update_file_status(self, file_id, status, commit=True):
        """
        Update the status of a file.

        Parameters
        ----------
        file_id : int
            The id for the file of interest.
        status : FileStatus
            The new status for the file record.
        commit : bool
            Determines whether changes will be committed to the database.
            This may be false if the changes are part of a transaction.

        Exceptions
        ----------
        ValueErrors will be thrown if the parameters cannot be validated.
        """
        self._validate_file_id(file_id)
        self._validate_status(status)

        sql = """
        UPDATE File
        SET file_status_id = :file_status_id
        WHERE id = :file_id
        """

        cursor = self._conn.cursor()
        cursor.execute(sql, {
            'file_id': file_id,
            'file_status_id': status.value
        })

        if commit:
            self._conn.commit()

    def has_errors(self):
        """Determine whether errors occurred.  Returns boolean."""
        sql = """
        SELECT COUNT(*) AS error_count
        FROM File
        WHERE file_status_id = :file_status_id
        """
        cursor = self._conn.cursor()
        cursor.execute(sql, {'file_status_id': FileStatus.ERROR.value})
        row = cursor.fetchone()
        return bool(row['error_count'])

    def folder_has_errors(self, folder_path):
        """
        Determine whether a folder has errors.

        NOTE:   The status of the folder record can't be used instead of this
                function, since the main use of the function is to determine
                whether it is appropriate to update the folder status.

        Parameters
        ----------
        folder_path : str
            The local path to the folder of interest.

        Returns
        -------
        A boolean indicating whether any files within the folder could not be
        uploaded successfully.
        """
        sql = """
        SELECT COUNT(*) AS error_count
        FROM File
        WHERE
            file_status_id <> :success_id
            AND
            path LIKE :path_like
            AND
            path <> :path
        """
        cursor = self._conn.cursor()
        cursor.execute(sql, {
            'success_id': FileStatus.SUCCESS.value,
            'path_like': f"{folder_path}%",
            'path': folder_path,
        })
        row = cursor.fetchone()
        return bool(row['error_count'])

    def get_retry_paths(self):
        """
        Get paths to be used to retry failed uploads.

        Only top-level paths are needed, since the upload process is
        recursive.

        Generates
        ---------
        Top level paths with statuses other than 'success'.
        """
        sql = """
        SELECT path
        FROM File
        WHERE
            is_top_level IS TRUE
            AND
            file_status_id <> :success_id
        """
        cursor = self._conn.cursor()
        cursor.execute(sql, {'success_id': FileStatus.SUCCESS.value})
        for row in cursor.fetchall():
            yield row['path']

    def get_errors(self):
        """
        Get a data frame containing upload errors.

        Returns
        -------
        A pandas.DataFrame containing the following columns:
         -  path (local system file path)
         -  error (description of the error)
         -  occurred_on (a timestamp for when the error occurred)
        """
        sql = """
        SELECT
            File.path,
            Error.error,
            FileError.occurred_on
        FROM
            FileError
            INNER JOIN File ON FileError.file_id = File.id
            INNER JOIN Error ON FileError.error_id = Error.id
        WHERE
            File.file_status_id = :file_status_id
        ORDER BY
            File.path
        """
        df_errors = pd.read_sql_query(
            sql=sql,
            con=self._conn,
            params={'file_status_id': FileStatus.ERROR.value}
        )
        return df_errors

    def get_log(self):
        """
        Get a log of files that were successfully uploaded.

        Folders will not be included in the log.

        Returns
        -------
        A pandas.DataFrame containing the following columns:
         -  path (local system file path)
        """
        sql = """
        SELECT path
        FROM File
        WHERE
            file_status_id = :file_status_id
            AND
            is_folder = FALSE
        ORDER BY path
        """
        df_log = pd.read_sql_query(
            sql=sql,
            con=self._conn,
            params={'file_status_id': FileStatus.SUCCESS.value}
        )
        return df_log

    def _validate_file_id(self, file_id):
        if not isinstance(file_id, int) or file_id < 0:
            raise ValueError("file_id must be a positive integer")

    def _validate_status(self, status):
        if not isinstance(status, FileStatus):
            raise ValueError("status must be an instance of upload_db.FileStatus")

    def _validate_path(self, path):
        # TODO:  Consider supporting mount points.
        if os.path.ismount(path):
            raise ValueError("mount points are not supported")

        if os.path.islink(path):
            raise ValueError("symbolic links are not supported")

        if not os.path.exists(path):
            raise ValueError(f"'{path}' is not a valid path")

    def _validate_top_level(self, top_level):
        if not isinstance(top_level, bool):
            raise ValueError('top_level must be a bool')


class ErrorManager:
    """
    Manager class for database operations relating to error records.

    (Basically, a collection of functions with shared access to a database
    connection object.)

    Parameters
    ----------
    db_connection : sqlite3.Connection
        A connection to the database of interest.
    file_manager : FileManager
        A similar object relating to file records.  See FileManager class
        definition, above.

    Exceptions
    ----------
    Value errors will be thrown if the parameters cannot be validated.
    """

    def __init__(self, db_connection, file_manager):
        if not isinstance(db_connection, sqlite3.Connection):
            raise ValueError("db_connection must be a sqlite3 connection")

        if not isinstance(file_manager, FileManager):
            raise ValueError("file_manager must be a FileManager object")

        self._conn = db_connection
        self._file_manager = file_manager

    def get_error(self, error_text):
        """
        Get an error record given the text of an error.

        Parameters
        ----------
        error_text : str
            The text of an error.

        Returns
        -------
        A row object (see sqlite3.Row) corresponding to the error record of
        interest, or None if no such record can be found.

        Exceptions
        ----------
        A ValueError will be thrown if the error text cannot be validated.
        """
        self._validate_error(error_text)
        sql = "SELECT * FROM Error WHERE error = :error"
        cursor = self._conn.cursor()
        cursor.execute(sql, {'error': error_text})
        row = cursor.fetchone()

        if not row:
            return None

        return row

    def create_error(self, error, commit=True):
        """
        Create an error record in the database.

        Parameters
        ----------
        error_text : str
            Text describing an error that occurred.
        commit : bool
            Determines whether changes will be committed to the database.
            This may be false if the changes are part of a transaction.

        Returns
        -------
        The id for the error record, or None if an input validation error is
        encountered.

        Exceptions
        ----------
        -   ValueErrors will be thrown if the parameters cannot be validated.
        -   An IntegrityError will be thrown if the insert query violates
            integrity constaints.  (This may happen if a record already exists
            for the error, due to a uniqueness constraint.)
        """
        self._validate_error(error)
        sql = """INSERT INTO Error (error) VALUES (:error)"""
        cursor = self._conn.cursor()
        cursor.execute(sql, {'error': error})
        error_id = cursor.lastrowid

        if commit:
            self._conn.commit()

        return error_id

    def get_error_from_id(self, error_id):
        """Get an error record given an error id."""
        self._validate_error_id(error_id)
        sql = "SELECT * FROM Error WHERE id = :error_id"
        cursor = self._conn.cursor()
        cursor.execute(sql, {'error_id': error_id})
        row = cursor.fetchone()
        if not row:
            return None
        return row

    def add_error_to_file(self, error_id, file_id, commit=True):
        """
        Assign an error to a file.

        Parameters
        ----------
        error_id : int
            The id for the error to be assigned.
        file_id : int
            The id for the file to which the error should be assigned.
        commit : bool
            Determines whether changes will be committed to the database.
            This may be false if the changes are part of a transaction.

        Returns
        -------
        The id of the record in the junction table (FileError) that associates
        the error with the file.

        Exceptions
        ----------
        ValueErrors will be thrown if the parameters cannot be validated.
        """
        self._validate_error_id(error_id)
        self._validate_file_id(file_id)
        sql = """
        INSERT INTO FileError (file_id, error_id)
        VALUES (:file_id, :error_id)
        """
        cursor = self._conn.cursor()
        cursor.execute(sql, {'error_id': error_id, 'file_id': file_id})
        file_error_id = cursor.lastrowid

        file_ = self._file_manager.update_file_status(
            file_id=file_id,
            status=FileStatus.ERROR,
            commit=False
        )

        if commit:
            self._conn.commit()

        if self._verbose:
            error = self.get_error_from_id(error_id)
            file_ = self._file_manager.get_file(file_id)
            print(f"ERROR for {file_['path']}: {error['error']}")

        return file_error_id

    def _validate_error(self, error):
        if not isinstance(error, str) or not error:
            raise ValueError("error must be a non-empty string")

    def _validate_error_id(self, error_id):
        if not isinstance(error_id, int) or error_id <= 0:
            raise ValueError("error_id must be a positive integer")

    def _validate_file_id(self, file_id):
        if not isinstance(file_id, int) or file_id <= 0:
            raise ValueError("file_id must be a positive integer")
