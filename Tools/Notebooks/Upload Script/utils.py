import os
import platform


ID_PATH_SEP = ','
PATH_SEP = '/'
WINDOWS_PATH_PREFIX = '//?/'
FOLDER_TYPE = 'Folder'


def get_active_path(path):
    """Get a path that can be used to get a file size, read/write a file, etc."""
    if platform.system() != 'Windows':
        return path

    path = path.lstrip(WINDOWS_PATH_PREFIX)
    return f'{WINDOWS_PATH_PREFIX}{path}'.replace(PATH_SEP, os.path.sep)
